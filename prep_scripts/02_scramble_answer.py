#!/usr/bin/env python3

from binascii import hexlify
from collections import defaultdict
import typer

def scramble_answer(groups_n: int, input_file: str = "answer.txt", output_file: str = "scrambledanswer.txt"):
    """Scrambles the answer file using N groups."""
    result = defaultdict(str)
    with open(input_file) as file:
        data = file.read()
    # Make sure the data is always padded to make unscrambling much easier
    data += " " * (groups_n - (len(data) % groups_n)) 
    data = "".join([data[i::groups_n] for i in range(0, groups_n)]).encode("utf-8")
    data = hexlify(data)
    with open(output_file, "w") as file:
        file.write(data.decode("utf-8"))


if __name__ == '__main__':
    typer.run(scramble_answer)
