#!/usr/bin/env python3

from binascii import hexlify
import json
import typer

def generate_hexstrings(groups_n: int, input_file: str, output_file: str = "hexdata.json"):
    """This function generates N hexstrings given an input file containing
    text; and saves them to a json file."""
    with open(input_file) as file:
        data = file.read()
    data = data.encode("utf-8")
    base = 170
    result = {}
    for group in range(groups_n):
        result[f"group{group:02}"] = hexlify(b"".join([chr(c + base + group).encode("utf-8") for c in data])).decode("utf-8")
    
    with open(output_file, "w") as file:
        json.dump(result, file, indent=4)


if __name__ == '__main__':
    typer.run(generate_hexstrings)
