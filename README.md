IceBreaker Workshop
===================

This is an icebreaker workshop to take with the class as an introduction.

- [IceBreaker Workshop](#icebreaker-workshop)
- [Prerequisite](#prerequisite)
- [The workshop](#the-workshop)
  - [Step 1: The answer API](#step-1-the-answer-api)
  - [Step 2: unscramble my answers](#step-2-unscramble-my-answers)

# Prerequisite

The class needs to prepare a list of questions for me. They can ask what they
want.

# The workshop

It's an Advent-of-Code-like exercise in two steps. The class if divided into
groups.

## Step 1: The answer API

Each group is given a long hexadecimal string as the input. Their goal is to
transform the hexstring into a bytestring (i.e. by using `binascii.unhexlify`).

Once that's done, they need to transform the bytestring into a list of `int`s
(i.e. with `[c for c in bytestring]`).

Once that's done, each group needs to transform the list of `int`s into a single
`int` via the following algorithm:

- starting with `answer = 0`
- for each int `N` in the list
  - if `N > (answer // 10)`
    - then `answer += N`
  - otherwise
    - `answer //= N`

Once they have the answer, they need to send an HTTP POST request to a the
workshop API `/answer` endpoint. Each group is given a string identifier as
well, and that should be present in the request's headers as `group-name`. The
answer itself is to be sent as a query parameter called `answer`.

## Step 2: unscramble my answers

Once each group has answered step 1 correctly, the text with my answers to their
questions can be found via the workshop API at the `/fetch` endpoint.

The text is hexed, so here as well `binascii.unhexlify` is of help.

The text is also scambled via the following algorithm:

- Given the text and a number of groups N
- each Nth letter of the text is appended in order to the Nth group
- the groups are concatenated in order
- EXAMPLE:
  - text = "This is an example"
  - groups = 3
  - result (_ indicates a space for clarity):
    1. T s s n x p
    2. h _ _ _ a l
    3. i i a e m e
   
    "Tssnxph___aliiaeme"
