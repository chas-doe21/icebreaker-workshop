"""This is an example solution for part 1 of the workshop.

Given a hexstring, transform it first into a list of integers, and then into a
single integer according to the instructions.

This example assumes that the hexstring is given via stdin.
"""

import sys
from binascii import unhexlify, Error

if __name__ == '__main__':
    # Read the incoming data from stdin, and remove eventual leading or trailing
    # whitespace
    hex_data = sys.stdin.read().strip()

    # Unhexlify the data
    try:
        bin_data = unhexlify(hex_data)
    except Error as e:
        print(f"'{hex_data}'")
        raise e

    # Transform the bytestring into a list of int. Every byte, if read
    # singularly, gives back an int
    bin_data = [b for b in bin_data]
    # The above line of code is equivalent to:
    #
    # new_bin_data = []
    # for b in bin_data:
    #     new_bin_data.append(b)
    # bin_data = new_bin_data
    #
    # but more concise and avoids using a temporary variable. That one-liner is
    # called list comprehension. Read more about it here
    # https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions

    # Implement the little algorithm to transform a list of ints into a single
    # int, as follows:
    #
    # Given answer = 0
    #   for each int N in our list
    #     if   N > answer // 10 then answer +=
    #     elif N <= answer then answer //= N
    answer = 0
    for n in bin_data:
        if n > answer//10:
            answer += n
        else:
            answer //= n
    print(f"Answer: {answer}")
