"""This is an example solution for part 2 of the workshop.

Given the answer, post it to the API to unlock your part of the solution.
"""

import requests

if __name__ == '__main__':
    # Set the known data
    answer = 42
    group_name = "group99"

    # In this example I used the `requests` library to carry out the HTTP
    # request. There are other libraries to choose from, i.e. `httpx` or even the
    # standard library (`httplib`).
    resp = requests.post(f"https://chas-intro.grinton.dev/answer?answer={answer}", 
                         headers={"group-name": group_name})

    print(resp.json())
