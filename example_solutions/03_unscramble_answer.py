"""This is an example solution for part 3 of the workshop.

Given the final answer and the number of groups, it unscrambles it.

You can always consider the input text to be padded with spaces at the end so
that len(answer) % groups == 0
"""

from binascii import unhexlify
import sys

if __name__ == '__main__':
    # Set the known data
    answer = sys.stdin.read()
    groups = 6

    answer = unhexlify(answer).decode("utf-8")

    # How many groups will the result be composed of?
    result_groups = len(answer) // groups


    # Divide the answer into as many chunks as there are groups in the final
    # answer, where each Nth letter ends up in the Nth group.
    chunks = [answer[i::result_groups] for i in range(0, result_groups)]

    print("".join(chunks))

